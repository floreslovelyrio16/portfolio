/*====================================
// 	Main Navigation
======================================*/
$('main.wrapper').scroll(function() {
    $('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
});